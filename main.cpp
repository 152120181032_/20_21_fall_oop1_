#include<iostream>
#include<stdlib.h>
#include<fstream>
#define size 100

int sumfunc(int M[], int counter)
{
    int i = 1, sum = 0; /// Dizinin 0. indeksindeki eleman dosyadaki eleman sayisini belirttigi icin "i" yi 1 den baslattim.

    while (i <= counter)
    {
        sum += M[i];
        i++;
    }

    return sum;
}

int productfunc(int P[], int counter)
{
    int i = 1, mul = 1;  /// Dizinin 0. indeksindeki eleman dosyadaki eleman sayisini belirttigi icin "i" yi 1 den baslattim.
    while (i <= counter)
    { 
        mul *= P[i];
        i++;
    }
    return mul;
}

float averagefunc(int A[], int counter)
{
    float avg = 0.0;
    int i = 1, sum;

    sum = sumfunc(A, counter);
    avg = (float)sum / (float)counter;

    return avg;
}

int smallestfunc(int S[], int counter)
{
    int i = 1, small, a;
    small = S[i];

    while (i <= counter)
    {
        if (S[i] < small)
            small = S[i];
        i++;
    }
    a = small;

    return a;
}

using namespace std;
int main()
{
    ifstream dataFile;
    int A[size], number = 0, i = 0, counter = -1;

    dataFile.open("input.txt", ios::in);
    if (!dataFile)
    {
        cout << "The file was not opened...\n\n" << endl;
    }
    else
    {
        cout << "The file was opened...\n\n";
        while (dataFile >> number)
        {
            A[i] = number;

            /// counter i -1 e esitleyerek "input.txt" dosyasindaki ilk satirda bulunan sayiyi hesaplamiyoruz.
            counter++;
            i++;
        }
    }
    dataFile.close();

    if (counter == A[0])
    {
        cout << "Sum      is  " << sumfunc(A, counter) << endl;
        cout << "Product  is  " << productfunc(A, counter) << endl;
        cout << "Average  is  " << averagefunc(A, counter) << endl;
        cout << "Smallest is  " << smallestfunc(A, counter) << endl;
    }

    else
        cout << "Ilk satirda belirtilen miktarda integer yok...\n ";


    cout << "\n\n\n";
    system("pause");
}